﻿Imports MySql.Data.MySqlClient

Public Class MainForm
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        koneksi()
        showgrid()
        addCmbJurusan()
        addCmbUkm()
    End Sub

    Private Sub showgrid()
        Try
            Call koneksi()
            da = New MySqlDataAdapter("select * from mahasiswa", KoneksiMySql.conn)
            ds = New DataSet()
            da.Fill(ds, "mahasiswa")
            DataGridView1.DataSource = ds.Tables("mahasiswa")
            DataGridView1.ReadOnly = True

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Sub addCmbJurusan()
        Try
            Dim jurusan() As String = {"Teknik Informatika", "Teknik Mesin", "Akuntansi", "Administrasi", "Teknik Sipil"}
            cmbJurusan.MaxDropDownItems = jurusan.Length - 1
            For i = 0 To (jurusan.Length - 1)
                cmbJurusan.Items.Add(jurusan(i))
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub addCmbUkm()
        Try
            Dim ukm() As String = {"Futsal", "Musik", "Robotika", "Magic", "Wirausaha"}
            cmbUKM.MaxDropDownItems = ukm.Length - 1
            For i = 0 To (ukm.Length - 1)
                cmbUKM.Items.Add(ukm(i))
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            If lblNIM.Text = "" Or lblName.Text = "" Or cmbJurusan.Text = "" Or cmbUKM.Text = "" Then
                MessageBox.Show("Silahkan isi semua form")
            Else
                Call koneksi()
                Dim querysave As String = "insert into mahasiswa values ('" & txtNIM.Text & "', '" & txtName.Text & "','" & cmbJurusan.Text & "', '" & cmbUKM.Text & "' )"
                cmd = New MySqlCommand(querysave, conn)
                If (cmd.ExecuteNonQuery = 1) Then
                    MsgBox("Input Berhasil")
                    Call showgrid()
                    Call EmptyForm()
                Else
                    MsgBox("Input data gagal, pastikan NIM yang anda masukan benar")
                End If
            End If
        Catch
            Try
                MsgBox("Nim terduplikat")
                txtNIM.Focus()
                txtNIM.SelectAll()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Try
    End Sub

    Sub EmptyForm()
        txtNIM.Text = ""
        txtName.Text = ""
        cmbJurusan.Text = ""
        cmbUKM.Text = ""
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If txtNIM.Text = "" Then
                MsgBox("Silahkan pilih data yang akan anda hapus")
            Else
                If MessageBox.Show("Yakin akan dihapus ?", "", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    Call koneksi()
                    Dim queryDelete As String = "delete from mahasiswa where nim = '" + txtNIM.Text + "'"
                    cmd = New MySqlCommand(queryDelete, conn)
                    cmd.ExecuteNonQuery()
                    MsgBox("Data dihapus")
                    Call showgrid()
                    Call EmptyForm()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            Dim i, j As Integer
            i = DataGridView1.CurrentRow.Index
            txtNIM.Text = DataGridView1.Item(0, i).Value
            txtName.Text = DataGridView1.Item(1, i).Value
            cmbJurusan.Text = DataGridView1.Item(2, i).Value
            cmbUKM.Text = DataGridView1.Item(3, i).Value
        Catch ex As Exception
            MsgBox("Data yg anda pilih kosong, silahkan pilih data lain!")
        End Try
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        EmptyForm()
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            Call koneksi()
            Dim queryEdit As String = "update mahasiswa set nama = '" & txtName.Text & "', jurusan = '" & cmbJurusan.Text & "', ukm = '" & cmbUKM.Text & "' where nim = '" & txtNIM.Text & "'"
            cmd = New MySqlCommand(queryEdit, conn)
            cmd.ExecuteNonQuery()
            MsgBox("Data berhasil diupdate")
            Call showgrid()
            Call EmptyForm()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class
